# README #

This repository contains the publishing client code for reading a DHT11, DHT22 or AM2302 sensor and transmitting that to an MQTT broker.  
It is in progress and currently being updated. Tested and functional on Raspberry Pi 4B and Raspberry Pi Zero W.  

Dependencies:  
 * paho-mqtt  
 * adafruit_dht  
 * CircuitPython (sponsored by adafruit)  
 * python3  
  
I give others the right to download and run this set of files for personal use.  
All other rights remain reserved by the Author (Marcus Galeotti)