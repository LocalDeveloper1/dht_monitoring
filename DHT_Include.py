
import time
import datetime
import board
import adafruit_dht
from paho.mqtt import client as mqtt

PHYS_PINS = {
    
    0 : board.D0,
    1 : board.D1,
    2 : board.D2,
    3 : board.D3,
    4 : board.D4,
    5 : board.D5,
    6 : board.D6,
    7 : board.D7,
    8 : board.D8,
    9 : board.D9,
    10 : board.D10,
    11 : board.D11,
    12 : board.D12,
    13 : board.D13,
    14 : board.D14,
    15 : board.D15,
    16 : board.D16,
    17 : board.D17,
    18 : board.D18,
    19 : board.D19,
    20 : board.D20,
    21 : board.D21,
    22 : board.D22,
    23 : board.D23,
    24 : board.D24,
    25 : board.D25,
    26 : board.D26
}

#logging function
def log(filePath, logStr):
    try:
        with open(filePath, "a") as logFile:
            logFile.write(f"{datetime.datetime.now()}: {logStr}\n")
    except Exception as e:
        print(f"Exception while attempting to write to {filePath}: {e.args[0]}", flush=True)    

#set up paho.mqtt connection callback
def on_connect(client, userData, flags, rc):
    if rc == 0:
        log(userData, "Connected to MQTT Broker")
    else:
        log(userData, f"Failed to connect, return code {rc}")