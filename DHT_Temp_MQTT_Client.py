import time
import datetime
import board
import adafruit_dht
from paho.mqtt import client as mqtt
import sys
import os
import DHT_Include

#################################################################################################################################
# Author: Marcus Galeotti
####
# Purpose: This program reads from a DHT11, DHT22 or AM2302 series of temperature and humidity
#   sensors and publishes this information to an MQTT Broker
###
# Parameters: 
#   1. sensorType - The type of sensor in the set of DHT11, DHT22, or AM2302
#   2. sensorPin - The GPIO physical input pin number that the sensor is connected to
#   3. brokerIP - The IP address of the MQTT Broker. IPV4 and IPV6 as well as symbolic hostnames, such as localhost,
#       are supported
#   4. brokerPort - The port number to connect to at @brokerIP
#   5. topic - the topic, as a string, to publish messages on 
#   6. clientID - the id of this client as an alphanumeric string
#	7. logFilePath (optional) - The filesystem path to output log messages to. If not supplied, it defaults to logs.txt
#   8. pathToCertificateFile (optional) - The path to a certificate file to use for encryption.
#       If not supplied, this client will assume a non-encrypted connection. This is not recommended for 
#       production enviornments
#   9. insecureEncryption (optional) - An option to pass which tells this client whether to verify the hostname 
#       specified in the certificate file passed to this function. A certificate file must be passed for this option
#       to be recognized. If this is not passed, this option is ignored
###
# How to Shutdown the Client: SIGINT is a supported signal and will perform a graceful shutdown of the client
################################################################################################################################

## constants and global variables
useEncryption = False
insecureEncryption = False
certificatePath = ""

#set up command line argument number and position constants
PROGRAM_NAME_ARG_NUM = 1
PROGRAM_NAME_ARG_POS = PROGRAM_NAME_ARG_NUM - 1

SENSOR_TYPE_ARG_NUM = 2
SENSOR_TYPE_ARG_POS = SENSOR_TYPE_ARG_NUM - 1

SENSOR_INPUT_PIN_ARG_NUM = 3
SENSOR_INPUT_PIN_ARG_POS = SENSOR_INPUT_PIN_ARG_NUM - 1

BROKER_IP_ARG_NUM = 4
BROKER_IP_ARG_POS = BROKER_IP_ARG_NUM - 1

BROKER_PORT_ARG_NUM = 5
BROKER_PORT_ARG_POS = BROKER_PORT_ARG_NUM - 1

BROADCAST_TOPIC_ARG_NUM = 6
BROADCAST_TOPIC_ARG_POS = BROADCAST_TOPIC_ARG_NUM - 1

CLIENT_ID_ARG_NUM = 7
CLIENT_ID_ARG_POS = CLIENT_ID_ARG_NUM - 1

LOG_FILE_PATH_ARG_NUM = 8
LOG_FILE_PATH_ARG_POS = LOG_FILE_PATH_ARG_NUM - 1

SERVER_CERT_FILE_ARG_NUM = 9
SERVER_CERT_FILE_ARG_POS = SERVER_CERT_FILE_ARG_NUM - 1

SECURE_ENCRYPTION_ARG_NUM = 10
SECURE_ENCRYPTION_ARG_POS = SECURE_ENCRYPTION_ARG_NUM - 1

MIN_NUM_OF_ARGS = 7

#main processing below
if(len(sys.argv) < MIN_NUM_OF_ARGS):
    print("Usage: %program_name %sensorType %sensorPin %brokerIP %brokerPort %topic %clientID [%logFilePath] [%pathToServerCertificateFile] [%insecureEncryption]")
    exit()
else:
    try:
        sensorType = sys.argv[SENSOR_TYPE_ARG_POS]
        dhtPin = DHT_Include.PHYS_PINS[int(sys.argv[SENSOR_INPUT_PIN_ARG_POS])]
        broker_ip = sys.argv[BROKER_IP_ARG_POS]
        broker_port = int(sys.argv[BROKER_PORT_ARG_POS])
        topic = sys.argv[BROADCAST_TOPIC_ARG_POS]
        client_id = sys.argv[CLIENT_ID_ARG_POS]
    except TypeError as e:
        print(f"A parameter was not the correct type: {e.args[0]}", flush=True)
        exit()
    except ValueError as e:
        print(f"A parameter was not the correct type: {e.args[0]}", flush=True)
        exit()

#set optional parameters if specified
if(len(sys.argv) >= LOG_FILE_PATH_ARG_NUM and (len(sys.argv[LOG_FILE_PATH_ARG_POS]) > 0)):
    logFilePath = str(sys.argv[LOG_FILE_PATH_ARG_POS])
else:
    print("Invalid log file path, defaulting to \"logs.txt\"")
    logFilePath = "./logs.txt"

if(len(sys.argv) >= SERVER_CERT_FILE_ARG_NUM and len(sys.argv[SERVER_CERT_FILE_ARG_POS]) > 0):
    certificatePath = sys.argv[SERVER_CERT_FILE_ARG_POS]
    useEncryption = True

    #set if hostname validation will take place (i.e. a self-signed certificate would be used)
    if(len(sys.argv) >= SECURE_ENCRYPTION_ARG_NUM):
        encryptStr = sys.argv[SECURE_ENCRYPTION_ARG_POS].upper()
        if(encryptStr != "FALSE" and encryptStr != "TRUE" ):
            print("Invalid %insecureEncryption passed; defaulting to trying to use a secure encryption...")
            insecureEncryption = False
        elif(encryptStr == "TRUE"):
            insecureEncryption = True
            DHT_Include.log(logFilePath, "Insecure encryption used; hostname verification not enabled.")
        else:
            insecureEncryption = False
else:
    print("Invalid certificate file path passed, defaulting to insecure connection!", flush=True)
    DHT_Include.log(logFilePath, "Insecure encryption used; hostname verification not enabled.")

client = mqtt.Client(client_id = client_id, userdata = logFilePath)
if(useEncryption):
    client.tls_set(certificatePath)
    client.tls_insecure_set(insecureEncryption)

#set on_connect callback
client.on_connect = DHT_Include.on_connect
try:
    client.connect(broker_ip, broker_port)
except Exception as e:
    DHT_Include.log(logFilePath, e.args[0])
    DHT_Include.log(logFilePath, "Unable to connect to broker. Client will exit...")
    print("Unable to connect to broker. Client will exit...")
    exit()


#initialize dhtDevice
if(sensorType.upper() == "DHT22" or sensorType.upper() == "AM2302"):
    dhtDevice = adafruit_dht.DHT22(dhtPin)
else:
    dhtDevice = adafruit_dht.DHT11(dhtPin)

#main loop, start reading and transmitting to broker
client.loop()
try:
    sleepDur = 2.0
    while(client.is_connected()): #this may be changed later to allow a disconnect message to be received from broker
        try:
            client.loop()
            dhtDevice.measure
            client.publish(topic, str(dhtDevice.temperature) + "," + str(dhtDevice.humidity))
            time.sleep(sleepDur)
        except RuntimeError as e:
            time.sleep(sleepDur)
        except Exception as e:
            DHT_Include.log(logFilePath, e.args[0])
except KeyboardInterrupt:
    print("Shutting down client...")
    DHT_Include.log(logFilePath, "KeyboardInterrupt Recieved. Program was shut down interactively.")
finally:
    client.disconnect()
    dhtDevice.exit()
    DHT_Include.log(logFilePath, "Client was shut down normally")
